<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.layout.master');
});

Auth::routes();
Route::group(['prefix' => 'dashboard','namespace'=>'admin',"middleware"=>'auth'],function(){



    Route::resource('services',"ServiceController");
    Route::resource('clients',"client_opinions_Controller");
    Route::get("/photoes/create","photoesController@create");
    Route::post("/photoes/store","photoesController@store")->name('photoes.store');
    Route::get('/photoes',"photoesController@index");

    Route::delete('/photoes/{id}',"photoesController@delete")->name("photoes.destroy");

});

Route::get('/home', 'website@show')->name('home');


