

<div class="form-group">
    <label class="col-md-2 control-label">الاسم</label>
    <div class="col-md-10">

        {!! Form::text("name",null,["class"=>"form-control"]) !!}
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">الوصف</label>
    <div class="col-md-10">
        {!! Form::textarea("description",null,["class"=>"form-control"]) !!}

    </div>
</div>
