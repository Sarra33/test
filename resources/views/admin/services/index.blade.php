@extends('admin.layout.master')

@section('styles')
@endsection


@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>

                </div>

                <h4 class="header-title m-t-0 m-b-30">Buttons Example</h4>

                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>

                        <th>الاسم</th>

                        <th>الوصف</th>

                        
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($services as $service)
                    <tr>
                      <td>{{$service->name}} </td>

                        <td>{{$service->description}} </td>



                        <td>
                            <a href="/dashboard/services/{{$service->id}}/edit" class="btn btn-primary">تعديل</a>

                        </td>
                        <td>{!! Form::open(["route"=>["services.destroy",$service->id],"method"=>'DELETE']) !!}


                            <button type="submit" class="btn btn-danger">Delete</button>
                            {!! Form::close() !!}</td>




                    </tr>
                        @empty
                        <tr>
                            <td></td>
                            <td></td>
                            <td> </td>
                            <td> </td>
                            <td></td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->

@endsection



@section('scripts')
@endsection
