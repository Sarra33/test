<div class="form-group">
    <label class="col-md-2 control-label">الإسم</label>
    <div class="col-md-10">
        {!! Form::text('name',null,["class"=>'form-control']) !!}

    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label" for="example-email">البريد الإلكتروني</label>
    <div class="col-md-10">
        {!! Form::email("email",null,["class"=>'form-control']) !!}

    </div>
</div>



<div class="form-group">
    <label class="col-md-2 control-label">كلمة المرور</label>
    <div class="col-md-10">
        {!! Form::input("password","password",null,["class"=>"form-control"]) !!}
       </div>
</div>
<div class="form-group">
    <label class="col-md-2 control-label">تأكيد كلمة المرور</label>
    <div class="col-md-10">
        {!! Form::input("password","password_confirmation",null,["class"=>"form-control"]) !!}
    </div>
</div>