@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">



                <h4 class="header-title m-t-0 m-b-30">Input Types</h4>

                <div class="row">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session()->has('success'))
                        <div class="alert alert-success">
                            <h4>{{session()->get('success')}}</h4>
                        </div>
                    @endif
                        <div class="col-lg-6">

{{$user->name}}



                            {!! Form::model($user,['route'=>['Users.update',$user->id],"files"=>true,"method"=>"PATCH"]) !!}

                       @include("admin.users.form")
                            <div class="form-group">
                                <label class="col-md-2 control-label">الصلاحيات</label>
                                <div class="col-md-10">





                                    @foreach($permission_collec as $permission)

                                    @if(in_array($permission,$per))
                                        <div class="inline field">
                                            <div class="ui checkbox">
                                                <input type="checkbox" name="permission[]"

                                                       checked="checked"
                                                         value="{{$permission}}"
                                                      > {{$permission}}

                                            </div>
                                            @else
                                                <div class="inline field">
                                                    <div class="ui checkbox">
                                                        <input type="checkbox" name="permission[]"


                                                               value="{{$permission}}"
                                                               tabindex="0" >

                                                    </div>
                                            {{$permission}}
                                        </div>
                                            @endif
                                    @endforeach










                            </div>



                            <div class="form-group">

                                <div class="col-md-10">
                                    <button type="submit" class="btn btn-primary">حفظ</button>
                                </div>
                            </div>

                        </form>
                    </div><!-- end col -->



                </div><!-- end row -->

                            {!! Form::close() !!}
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->


@endsection
