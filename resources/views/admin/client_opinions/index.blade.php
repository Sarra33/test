@extends('admin.layout.master')

@section('styles')
@endsection


@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <div class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>
                   

                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>الإسم</th>
                        <th>الراي</th>
                        <th>الصورة</th>
                        <th> حذف</th>
                        <th>تعديل</th>


                    </tr>
                    </thead>

                    <tbody>
                    @forelse($clients as $client)
                    <tr>
                        <td>{{$client->name}}</td>
                        <td>{{$client->opinion}}</td>

                        <td>
                            <img src="/images/{{$client->image}}" style="width: 100px; height: 100px; border-radius: 50%;">
                        </td>
                        <td>

                            <a href="/dashboard/clients/{{$client->id}}/edit" class="btn btn-primary">تعديل</a>  </td>
                        <td>
                            <form action="{{ route('clients.destroy',$client->id) }}" method="POST">




                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>

                            {{-- @endif--}}
                        </td>
                    </tr>
                        


                        @empty
                        <tr>
                            <td></td>
                            <td></td>
                            <td> </td>
                            <td> </td>
                            <td></td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->

@endsection



@section('scripts')
@endsection
