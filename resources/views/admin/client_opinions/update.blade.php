@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">



                <h4 class="header-title m-t-0 m-b-30">Input Types</h4>

                <div class="row">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session()->has('success'))
                        <div class="alert alert-success">
                            <h4>{{session()->get('success')}}</h4>
                        </div>
                    @endif
                        <div class="col-lg-6">

{{$client->name}}



                            {!! Form::model($client,['route'=>['clients.update',$client->id],"files"=>true,"method"=>"PATCH"]) !!}

                       @include("admin.client_opinions.form")
                            
                            <div class="form-group">

                                <div class="col-md-10">
                                    <button type="submit" class="btn btn-primary">تعديل</button>
                                </div>
                            </div>

                   {!! Form::close() !!}
                    </div><!-- end col -->



                </div><!-- end row -->

                            {!! Form::close() !!}
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->


@endsection
