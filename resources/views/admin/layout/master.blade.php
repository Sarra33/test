<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" >
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App Favicon -->
    @include("admin.layout.styles")
    @yield('styles')
    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{asset('/admin/assets/js/modernizr.min.js')}}"></script>

</head>


<body class="fixed-left widescreen">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->

    <!-- Top Bar End -->
@include("admin.layout.header")

    <!-- ========== Left Sidebar Start ========== -->
    @include('admin.layout.menu') <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <div class="content-page">
        <!-- Start content -->

        <h6>{{config("app.locale")}}</h6>
        <div class="content">
            <div class="container">


               @yield('content')
                <!-- end row -->

            </div> <!-- container -->

        </div> <!-- content -->

        @include('admin.layout.footer')

    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    <!-- Right Sidebar -->

    <!-- /Right-bar -->

</div>
<!-- END wrapper -->



<script>
    var resizefunc = [];
</script>
 @include("admin.layout.scripts")
@yield('scripts')
<!-- jQuery  -->


</body>
</html>
