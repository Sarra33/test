@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">



                <h4 class="header-title m-t-0 m-b-30">Input Types</h4>

                <div class="row">


                    <div class="col-lg-6">
                      )<table id="datatable-buttons" class="table table-striped table-bordered">
                            <thead>
                            <tr>

                                <th>الصورة</th>

                            </tr>
                            </thead>

                            <tbody>
                            @forelse($photoes as $photo)
                                <tr>
                                   <td>
                                        <img src="/images/{{$photo->name}}" style="width: 100px; height: 100px; border-radius: 50%;">
                                    </td>

                                    <td>

                                        {{-- <a href="/dashboard/clients/{{$client->id}}/edit" class="btn btn-primary">تعديل</a>--}}
                                        <form action="{{ route('photoes.destroy',$photo->id) }}" method="POST">




                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>

                                        {{-- @endif--}}
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td></td>

                                </tr>
                            @endforelse

                            </tbody>
                        </table>

                    </div><!-- end col -->



                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->


@endsection
