<div id="sec3" class="sec ">
    <div class="con_01">
        <h2  class="ribbon-title--link">خدماتنا</h2>
        <div class="container">
            <div class="row">

                @foreach($services as $service)
                <div class="col-sm-4 bounceInRight wow"  data-wow-delay="0s" data-wow-duration="2s" >
                    <div class="iconbox">
                        <div class="icon">
                            <i class="fas fa-hands-helping" aria-hidden="true"></i>
                        </div>
                        <p class="serv-title"> {{$service->name}}</p>
                        <p>{{$service->description}}</p>
                    </div>
                </div>
                    @endforeach

            </div>
        </div>
    </div>
</div>