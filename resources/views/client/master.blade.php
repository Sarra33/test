<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('client/assets/img/raghwa-app/logo.png')}}">
    <title>زمزم</title>
    <!-- bootstrab -->
    @include("client.styles");

</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">
<!-- -----------------loading------------------------------ -->
<!-- nav bar -->
<!-- nav -->
@include('client.header')
<!-- /nav -->
@include('client.about')
<!-- sec2 slider-img  -->
@include('client.images_slider')
<!-- sec3 services -->
@include('client.services_slider')
<!-- sec4 testemonials  -->
@include('client.testemonials')
<!-- sec5 download -->
<!-- drop -->
@include('client.download')
<!-- /drop -->
<!-- ---------------footer-------------- -->
@include('client.footer')
<!-- loader -->
<div class="load-content">
    <div class="loader">
        <span class="loader__inner"></span>
        <span class="loader__inner"></span>
    </div>
</div>
<!-- ------------------top------------------------------------ -->
<div id="top">
    <i class="fas fa-arrow-up"></i>
</div>
<!-- /top -->
<!-- bootstrab -->
@include('client.scripts')
</body>
</html>