<script src="{{asset('client/assets/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('client/assets/js/popper.min.js')}}"> </script>
<script src="{{asset('client/assets/js/bootstrap.min.js')}}"></script>
<!-- OwlCarousel -->
<script src="{{asset('client/assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('client/assets/js/wow.min.js')}}"></script>
<!-- script -->
<script src="{{asset('client/assets/js/script.js')}}"></script>
