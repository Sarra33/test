<link rel="stylesheet" href="{{asset('client/assets/css/bootstrap.min.css')}}">
<!-- owel carsoul -->
<link rel="stylesheet" href="{{asset('client/assets/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('client/assets/css/owl.theme.default.min.css')}}">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<!-- animate -->
<link rel="stylesheet" href="{{asset('client/assets/css/animate.css')}}">
<!-- style -->
<link rel="stylesheet" href="{{asset('client/assets/css/style.css')}}">