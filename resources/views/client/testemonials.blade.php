<div id="sec4" class="sec">
    <div class="container-fluid">
        <h2  class="ribbon-title--link">اراء عملائنا</h2>
        <div class="row">

            @foreach($clients as $client)
            <div class="col-md-3 col-12 col-sm-6 service-box-container ">

                <div class="service-box">
                    <div class="service-icon yellow">
                        <div class="front-content">
                            <img  src="images/{{$client->image}}">
                            <h3>{{$client->name}}</h3>
                        </div>
                    </div>

                    <div class="service-content">
                        <p> {{$client->opinion}} </p>
                    </div>
                </div>
            </div>

                @endforeach

        </div>
    </div>
</div>