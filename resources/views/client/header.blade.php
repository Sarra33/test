<nav class="navbar navbar-expand-lg navbar-light bg-light sect3" id="navbar-example2">
    <a class="navbar-brand" href="#" >
        <img src="img/raghwa-app/logo.png" class="logo">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" >
        <span class="navbar-toggler-icon" style=""></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent" >
        <ul class="navbar-nav mr-auto nav-pills">
            <li class="nav-item " >
                <a class="nav-link " href="#sec1" >الصفحة الرئيسية<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="#sec2" >الصور<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="#sec3">خدماتنا <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="#sec4" >اراء العملاء <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item  " >
                <a class="nav-link" href="#sec5" >التحميل <span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>