<?php

namespace App\Http\Controllers\Admin;

use App\clients;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
class client_opinions_Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $clients=clients::all();
         return view('admin.client_opinions.index')->with(["clients"=>$clients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.client_opinions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs=$request->all();
        // return ($inputs['permission']);

        $rules = [
            'name'=>'required|string|max:191',
            'opinion'=>'required|string',

        ];
        $this->validate($request,$rules);


        if($file=$request->file('image'))
        {
            $name="client".$file->getClientOriginalName();
       $file->move("images",$name);
            $inputs['image']=$name;

        }




       clients::create($inputs);


        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client=clients::find($id);
        return  view ("Admin.client_opinions.update",compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs=$request->all();

//
//dd($request);
        $rules = [
            'name'=>'required|string|max:191',
            'opinion'=>'required|string',

        ];
        $this->validate($request,$rules);
          if( $file=$request->file('image'))
          {


            $name="client".$file->getClientOriginalName();
            $file->move("images",$name);
            $inputs['image']=$name;

          }
  clients::find($id)->update($inputs);

          return back();



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
