<?php

namespace App\Http\Controllers\admin;

use App\permission;
use App\role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use PhpParser\Node\Stmt\Return_;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  //return response(Auth::user()->permissions->where("name",'=','add'));

        $users =User::all();
     //  return response($users);

        return view('admin.users.index')->with(['users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  $inputs=$request->all();
       // return ($inputs['permission']);

        $rules = [
            'name'=>'required|string|max:191',
            'email'=>'required|email|unique:users,email',
            'password'=>'required|string|confirmed',
        ];
        $this->validate($request,$rules);


          if($file=$request->file('image'))
        {
            $name="user".$file->getClientOriginalName();
            $file->move("images",$name);
            $inputs['image']=$name;

        }



        $inputs['password'] = Hash::make($request->password);

       $user= User::create($inputs);



}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $user=User::find($id);

return view("admin.users.update",compact(["user"]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      // return response($request->all());
       $rules = [
         'name'=>'required|string|max:191',
         'email'=>'required|email|unique:users,email',

       ];
       $this->validate($request,$rules);
        $inputs = $request->all();
        if($file=$request->file('image'))
        {
            $name="user".$file->getClientOriginalName();


          uploader($name);
            $inputs['image']=$name;

        }
      //  $inputs['password'] = Hash::make($request->password);


$_names=[];
        $_user=User::find($id);

       $user= User::find($id)->update(["name"=>$inputs['name'],"email"=>$inputs['email'],"role"=>$inputs['role']]);
     /*  $u_permissions=$_user->permissions;
     //  return response($u_permissions);
       foreach($u_permissions as $p)
       {   
           return $p->name;

           return response($_names);
       }*/


      //return response($_names);


       // $user->roles()->sync($ids_coll);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::find($id)->delete();
    }
}
