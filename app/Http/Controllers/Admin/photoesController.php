<?php

namespace App\Http\Controllers\Admin;

use App\photos;
use Illuminate\Http\Request;
use App\Http\Controllers\controller;
class photoesController extends Controller
{
    public function create(){

        return view ("admin.photoes.create");
    }
   public function index(){
          $photoes=photos::all();
        return view ('admin.photoes.index',compact('photoes'));

   }

    public function store( Request $request){
          $inputs=$request->all();
        if($file=$request->file('name'))
        {
            $name=$file->getClientOriginalName();
            $file->move("images",$name);
            $inputs['name']=$name;

        }
photos::create($inputs);


        return back();

    }


    public function delete($id){

         $name=photos::find($id)->name;
        $photo=photos::destroy($id);
        unlink( public_path("images/".$name));

        return back();

    }
}
